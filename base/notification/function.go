package notification

//func:show how registered method be used by users
//realize:check every user in db and analysis user.PreferredNoticeMethod and print the result
func ShowUsedMethod() {

}

//func:delete the method in RegisteredNoticeMethod by name
//realize:traverse the slice RegisteredNoticeMethod and check if the name is in
// 			if so :remove ;
//			if not :do nothing and print log
func DeleteRegistedMethod(name string) {

}


//func init() {
//	event.RegisterListener("register_router", func(e *echo.Echo) {
//		e.POST("/email_send_message", ...)
//	})
//}
