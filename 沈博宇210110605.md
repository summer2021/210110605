# EDUOJ项目报告

## 项目信息

### 项目名称： 

通知系统

### 方案描述： 

目前的EduOJ缺乏一个通知系统。我们期望实现一个可由用户配置渠道偏好的通知系统，具体需求如下：

1. 实现通知渠道偏好设置。用户可设置其偏好的通知渠道。 
2. 设计通知接口，该接口每次调用时，给指定用户，按其偏好渠道发送一条通知。本接口需在通知系统中实现，供通知发送者调用。 
3. 设计通知渠道接口，该接口每次调用时，给指定用户，从特点渠道发送一条通知。本接口由他人实现，在通知系统中调用。 
4. 注：接口请在proposal中完成设计，在项目开始后开会讨论修改，并决定使用的最终版本

### 时间规划： 

#### 已完成工作： 

已完成系统设计工作，面向渠道开发者的设计文档，代码主体框架和部分功能

具体内容如下：

#### EduOJ通知系统设计方案

- ##### 数据库设计

  对user表新增加两列

  原有数据库设计：

  ```go
  type User struct {
  	ID       uint   `gorm:"primaryKey" json:"id"`
  	Username string `gorm:"unique_index" json:"username" validate:"required,max=30,min=5,username"`
  	Nickname string `gorm:"index:nickname" json:"nickname"`
  	Email    string `gorm:"unique_index" json:"email"`
  	Password string `json:"-"`
  
  	Roles      []UserHasRole `json:"roles"`
  	RoleLoaded bool          `gorm:"-" json:"-"`
  
  	ClassesManaging []*Class `json:"class_managing" gorm:"many2many:user_manage_classes"`
  	ClassesTaking   []*Class `json:"class_taking" gorm:"many2many:user_in_classes"`
  
  	Grades []*Grade `json:"grades"`
  
  	CreatedAt time.Time      `json:"created_at"`
  	UpdatedAt time.Time      `json:"-"`
  	DeletedAt gorm.DeletedAt `sql:"index" json:"deleted_at"`
  	// TODO: bio
  
  	Credentials []WebauthnCredential
  }
  ```

  修改后的新表：

  ```go
  type User struct {
  	ID       uint   `gorm:"primaryKey" json:"id"`
  	Username string `gorm:"unique_index" json:"username" validate:"required,max=30,min=5,username"`
  	Nickname string `gorm:"index:nickname" json:"nickname"`
  	Email    string `gorm:"unique_index" json:"email"`
  	Password string `json:"-"`
  	PreferredNoticeMethod string `gorm:"preferred_notice_method"`
  	NoticeAccount string `gorm:"notice_account"`
  
  	Roles      []UserHasRole `json:"roles"`
  	RoleLoaded bool          `gorm:"-" json:"-"`
  
  	ClassesManaging []*Class `json:"class_managing" gorm:"many2many:user_manage_classes"`
  	ClassesTaking   []*Class `json:"class_taking" gorm:"many2many:user_in_classes"`
  
  	Grades []*Grade `json:"grades"`
  
  	CreatedAt time.Time      `json:"created_at"`
  	UpdatedAt time.Time      `json:"-"`
  	DeletedAt gorm.DeletedAt `sql:"index" json:"deleted_at"`
  	// TODO: bio
  
  	Credentials []WebauthnCredential
  }
  ```

  通知系统为通知渠道开发者预留了`user`表中两列内容，分别是：

  ```go
  	PreferredNoticeMethod string `gorm:"preferred_notice_method"`
  	NoticeAccount string `gorm:"notice_account"`
  ```

  这两列分别记录了用户的通知渠道偏好和通知渠道地址。

- #### 更新通知偏好

  修改了`UpdateMe`函数实现更新用户的通知属性功能。

  ```go
  func UpdateMe(c echo.Context) error {
  	user, ok := c.Get("user").(models.User)
  	if !ok {
  		panic("could not convert my user into type models.User")
  	}
  	if !user.RoleLoaded {
  		user.LoadRoles()
  	}
  	req := request.UpdateMeRequest{}
  	err, ok := utils.BindAndValidate(&req, c)
  	if !ok {
  		return err
  	}
  	count := int64(0)
  	utils.PanicIfDBError(base.DB.Model(&models.User{}).Where("email = ?", req.Email).Count(&count), "could not query user count")
  	if count > 1 || (count == 1 && user.Email != req.Email) {
  		return c.JSON(http.StatusConflict, response.ErrorResp("CONFLICT_EMAIL", nil))
  	}
  	utils.PanicIfDBError(base.DB.Model(&models.User{}).Where("username = ?", req.Username).Count(&count), "could not query user count")
  	if count > 1 || (count == 1 && user.Username != req.Username) {
  		return c.JSON(http.StatusConflict, response.ErrorResp("CONFLICT_USERNAME", nil))
  	}
  	user.Username = req.Username
  	user.Nickname = req.Nickname
  	user.Email = req.Email
  	user.PreferredNoticeMethod = req.PreferredNoticeMethod
  	type Noticejson struct {
  		PreferredNoticeMethod string
  		NoticeAccount string
  	}
  	noticejson := Noticejson{
  		PreferredNoticeMethod: req.PreferredNoticeMethod,
  		NoticeAccount: req.NoticeAccount,
  	}
  	noticejson_byte, err := json.Marshal(noticejson)
  	if err != nil {
  		println("could not creat json")
  	}
  	user.NoticeAccount = string(noticejson_byte)
  	utils.PanicIfDBError(base.DB.Omit(clause.Associations).Save(&user), "could not update user")
  	return c.JSON(http.StatusOK, response.UpdateMeResponse{
  		Message: "SUCCESS",
  		Error:   nil,
  		Data: struct {
  			*resource.UserForAdmin `json:"user"`
  		}{
  			resource.GetUserForAdmin(&user),
  		},
  	})
  }
  ```

  其中

  ```go
  user.PreferredNoticeMethod = req.PreferredNoticeMethod
  	type Noticejson struct {
  		PreferredNoticeMethod string
  		NoticeAccount string
  	}
  	noticejson := Noticejson{
  		PreferredNoticeMethod: req.PreferredNoticeMethod,
  		NoticeAccount: req.NoticeAccount,
  	}
  	noticejson_byte, err := json.Marshal(noticejson)
  	if err != nil {
  		println("could not creat json")
  	}
  	user.NoticeAccount = string(noticejson_byte)
  ```

  该部分可见，前端传来的`PreferredNoticeMethod`会被存入对应的数据库中`user`表中`PreferredNoticeMethod`一列，而前端传来的`PreferredNoticeMethod`和`NoticeAccount`会被序列化为`json`字符串存入`user`表中`NoticeAccount`列

  开发者在得到`user`的信息后，若想知道具体的渠道偏好与通知地址，可以调用`json.UnMarshal`将其反虚拟化解析

  函数签名：

  ```go
  func UpdateMe(c echo.Context) error {
      user, ok := c.Get("user").(models.User)
  }
  error可能报错原因：	无法由前端载入model.user
  					数据库响应错误
  					序列化生成json错误等
  ```

  

- #### 注册新的通知功能

1. 设计思路

   - 在`notification`包下的全局变量`RegisteredPreferredNoticeMethod`用于记录已经注册启用的通知模块

   - 在注册时应该校验传入数据，防止类似同名方式出现

     ```go
     func register(name string) {
     	RegisteredPreferredNoticedMethod = append(RegisteredPreferredNoticedMethod, name)
     	//..
     }
     ```


- #### 修改现有的已经注册的通知功能

1. 设计思路

   - 查询得到`RegisteredPreferredNoticeMethod`中需要修改的字段

   - 重新赋值

   - 对整个`user`表进行遍历，更新每个使用原有通知渠道的用户为新渠道

     当然这部分还包含一定设计细节

     如是否直接对用户数据库进行修改、修改前后是否应该以及用什么方式通知系统内的修改。

     ```go
     func UpdateMethod(oldname string, newname string) error{
     
     }
     ```

     

- #### 展示现有的已经注册的通知方式

1. 设计思路

   - 简单遍历输出`RegisteredPreferredNoticeMethod`

     ```go
     func ShowRegisteredMethod() {
     
     }
     ```

     全局变量，无需传参

- #### 展示各个通知方式的使用情况

1. 设计思路

   - 遍历数据库表`user`，记录每个用户的`PreferredNoticeMethod`，将该json字段使用`json.Unmarshal`解析存入一个结构

   - 输出通知方式使用用户数量

     ```
     func ShowUsedMethod(db *gorm,) error{
     
     }
     ```

     

- #### 删除禁用已经注册过的通知方式

1. 设计思路

   - 删除`RegisteredPreferredNoticeMethod`中部分字段

   - 对整个`user`表进行遍历，修改使用被删通知渠道的用户的通知渠道为默认渠道

   - 

     ```go
     func DeleteRegisteredMethod() {
     
     }
     ```

     全局变量，无需传参

- #### 得到现有已经注册过的通知方法个数

1. 设计思路

   遍历`RegisteredPreferredNoticeMethod`，输出已经注册的通知渠道个数

- #### 绑定用户方式

1. 设计思路

   - event模块通过go `reflect`来实现事务功能

   - 添加路由：调用函数`event.FireEvent`在`router`中触发“regist_router"事件

   - 使用时在init时应该调用`RegistListener`注册"register_router"

     ```go
     func init() {
     	event.RegisterListener("register_rouer", func(e *echo.Echo) {
     		e.POST("/bind_sms", ...)
     	})
     }
     ```


- #### 发送消息

  1. 设计思路

     - 参数设计：接收者，发送标题，发送内容，...

     - 查询接收者的`PreferredNoticeMethod`

     - 解析接收者的`NoticeDetail`得到收件地址

     - 触发新的事件：`XXX_send_message`，XXX代表通知渠道，事件应该由渠道开发者书写名称，如果名称不匹配，会造成FireEvent的panic，错误信息返回在err当中，向渠道开发者返回触发事件失败log，帮助其调整修复代码。

     - 若事件触发成功，但由于一些原因，包含但不限于：邮箱被封、邮箱空号、网路问题。返回的报错信息会在函数返回值中呈现，应该对其解析理解，向系统使用者报出具体的失败原因。

       ```go
       func SendMessage(receiver *models.User, title string, message string) {
       	method := receiver.PreferredNoticeMethod
       	result, err := event.FireEvent(fmt.Sprintf("%s_send_message", method), receiver, title, message)
       	if err != nil {
       		//panic
       		//事件不存在？
       	}
       	if mErr := result[0][0].(error); mErr != nil {
       		//手机号不存在？
       	}
       }
       ```

       



#### 未完成功能：

细化功能实现和代码填充，缺少测试

#### 遇到的问题及解决方案： 

前期对于项目理解不够全面，在与导师的交流中对于框架和设计思路更加明晰，导师开视频会议与我讲解交流项目细节

#### 后续工作安排： 

后期继续按照时间规划完成所有功能

